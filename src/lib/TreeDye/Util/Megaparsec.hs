{-|
Module      : TreeDye.Util.Colour
Description : Utilities for working with 'Colour's
Copyright   : © Antal Spector-Zabusky 2017–2018
License     : BSD3
Maintainer  : Antal Spector-Zabusky <antal.b.sz@gmail.com>

Utilities for working with
<https://hackage.haskell.org/package/megaparsec Megaparsec>.
-}

module TreeDye.Util.Megaparsec (simpleFailure) where

import qualified Data.Set as S
import Text.Megaparsec

-- |Signal an error in the parser at the given point with the given text.
-- Analogous to 'fail', but for any 'MonadParsec'.
--
-- Note that this will signal an error at the point in the input that it is
-- called; to register an error over a span of text, you will need to use
-- Megaparsec's full error-handling capabilities.
simpleFailure :: MonadParsec e s m => String -> m a
simpleFailure = fancyFailure . S.singleton . ErrorFail
