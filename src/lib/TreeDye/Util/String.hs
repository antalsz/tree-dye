{-|
Module      : TreeDye.Util.String
Description : Utilities for formatting strings
Copyright   : © Antal Spector-Zabusky 2017–2018
License     : BSD3
Maintainer  : Antal Spector-Zabusky <antal.b.sz@gmail.com>

Utilities for formatting strings.
-}

{-# LANGUAGE LambdaCase #-}

module TreeDye.Util.String (punctuateList, punctuateNEL, describeWithList) where

import Data.List.NonEmpty (NonEmpty(..))
import qualified Data.List.NonEmpty as NEL

-- |Module-local: Wrap a string in a list, unless it's empty.  Used to suppress
-- output of empty strings.
present :: String -> [String]
present "" = []
present s  = [s]

-- |Format a list for English-language human-facing output.  Takes as its arguments:
--
-- 1. The conjunction for the list (e.g., @"and"@, @"or"@);
-- 2. The list of items;
-- 3. The singular noun for an item in the list (e.g., @"item"@); and
-- 4. The plural noun for an item in the list (e.g., @"items"@).
--
-- It is easiest to understand 'describeWithList' by example.  To talk about the
-- color channels in an image, we might have any of the following:
--
-- >>> describeWithList "and" ["cyan", "magenta", "yellow", "black", "alpha"] "channel" "channels"
-- "cyan, magenta, yellow, black, and alpha channels"
-- >>> describeWithList "and" ["red", "green", "blue"] "channel" "channels"
-- "red, green, and blue channels"
-- >>> describeWithList "and" ["black", "white"] "channel" "channels"
-- "black and white channels"
-- >>> describeWithList "and" ["color"] "channel" "channels"
-- "color channel"
-- >>> describeWithList "and" [] "channel" "channels"
-- "no channels"
--
-- More completely: Given @describeWithList conj things one many@, concatenate
-- the elements of @things@ separated with commas and spaces, placing the
-- conjunction @conj@ before the last item.  If there are only two items, there
-- are no commas; if there is only one item, there are neither commas nor a
-- conjunction; and if there are no items, say "no".  Then append @one@ or
-- @many@, preceded by a space, depending on if there was one item or not in the
-- list (zero and more than one are handled identically).  If any of @conj@,
-- @one@, or @many@ is empty, it is ignored.
--
-- For more basic formatting of lists, see 'punctuateList'.
describeWithList :: String -> [String] -> String -> String -> String
describeWithList conj things one many =
  unwords $ punctuateList "," conj "no" things
          : present (case things of [_] -> one ; _ -> many)

-- |Format a non-empty list for English-language human-facing output.  Takes as
-- its arguments:
--
-- 1. The separator betweel list items (e.g., @","@, @";"@);
-- 2. The conjunction for the list (e.g., @"and"@, @"or"@); and
-- 3. The 'NonEmpty' list of items.
--
-- This uses the Oxford comma (or other separator).
--
-- This function is the same as 'punctuateList', but for 'NonEmpty' lists; see
-- its documentation for more detailed output examples and descriptions.
punctuateNEL :: String -> String -> NonEmpty String -> String
punctuateNEL sep conj = unwords . \case
    item  :| []      -> [item]
    item1 :| [item2] -> [item1] ++ present conj ++ [item2]
    items            -> map (++ sep) (NEL.init items)
                     ++ present conj
                     ++ [NEL.last items]

-- |Format a list for English-language human-facing output.  Takes as its
-- arguments:
--
-- 1. The separator betweel list items (e.g., @","@, @";"@);
-- 2. The conjunction for the list (e.g., @"and"@, @"or"@);
-- 3. The no-item case (e.g., @"nothing"@, @"missing list"@); and
-- 4. The list of items.
--
-- This uses the Oxford comma (or other separator).  It is easiest to understand
-- 'punctuateList' by example.  To talk about the color channels in an image, we
-- might have any of the following (assuming @OverloadedLists@ is enabled):
--
-- >>> punctuateList "," "and" "no channels" ["cyan", "magenta", "yellow", "black", "alpha"]
-- "cyan, magenta, yellow, black, and alpha channels"
-- >>> punctuateList "," "and" "no channels" ["red", "green", "blue"]:
-- "red, green, and blue"
-- >>> punctuateList "," "and" "no channels" ["black", "white"]
-- "black and white"
-- >>> punctuateList "," "and" "no channels" ["color"]
-- "color"
-- >>> punctuateList "," "and" "no channels" []
-- "no channels"
--
-- More completely: Given @puncutateList sep conj none items@, concatenate the
-- elements of @items@ separated with @sep@ and spaces, placing the conjunction
-- @conj@ before the last item.  There is no space before @sep@, but there is
-- before @conj@.  If there are only two items, @sep@ is not used; if there is
-- only one item, neither @sep@ nor @conj@ is used; and if there are no items,
-- @none@ is returned instead (and neither @sep@ nor @conj@ is used.)  If either
-- @sep@ or @conj@ is empty, it is ignored.
--
-- To format 'NonEmpty' lists, see 'punctuateNEL'; to format lists of
-- descriptors, see 'describeWithList'.
punctuateList :: String -> String -> String -> [String] -> String
punctuateList sep conj none = maybe none (punctuateNEL sep conj) . NEL.nonEmpty
